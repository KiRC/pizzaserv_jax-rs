
import logic.IBaseMethods;
import logic.OrdersEntity;
import logic.PizzaEntity;
import logic.UserEntity;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


import javax.jws.soap.SOAPBinding;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static sun.misc.Version.println;

public class BaseMethods implements IBaseMethods {

    public UserEntity addPhoneNumber(String userName, String phone) throws Exception {
        Session session = null;
        UserEntity ue = null;
            ue = userExist(phone);
            if(ue==null) {
                session = HibernateMain.getSession();
                session.beginTransaction();
                ue = new UserEntity();
                ue.setPhone(phone);
                ue.setUserName(userName);
                session.save(ue);
                session.getTransaction().commit();
                sessionCheck(session);
        }
        return ue;
    }

    private UserEntity userExist(String phone) throws Exception{
        Session session = null;
        UserEntity ue = null;
            session = HibernateMain.getSession();
            session.beginTransaction();
            Query query = session.createQuery(
                    "from UserEntity e where e.phone='"+phone+"'"
            );
            session.getTransaction().commit();
            List result = query.getResultList();
            if(!result.isEmpty())
                ue=(UserEntity)result.get(0);
        sessionCheck(session);
        return ue;
    }

    private PizzaEntity getPizzaByName(String pName) throws Exception{
        Session session = null;
        PizzaEntity pe = null;
        session = HibernateMain.getSession();
        session.beginTransaction();
            Query query = session.createQuery(
                    "from PizzaEntity pe where pe.name='"+pName+"'"
            );
            session.getTransaction().commit();
            List result = query.getResultList();
            if(!result.isEmpty())
                pe = (PizzaEntity)result.get(0);
        sessionCheck(session);
    return pe;
    }

    public UserEntity getUserByName(String userName) throws Exception{
        Session session = null;
        UserEntity userEntity = null;
        session = HibernateMain.getSession();
        session.beginTransaction();
        Query query = session.createQuery(
                "from UserEntity ue where ue.userName='"+userName+"'"
        );
        session.getTransaction().commit();
        List result = query.getResultList();
            userEntity = (UserEntity)result.get(0);
        sessionCheck(session);
        return userEntity;
    }

    public List<PizzaEntity> getPizzasList() {
        Session session = null;
        List<PizzaEntity> lpe = null;
        session = HibernateMain.getSession();
        session.beginTransaction();
        Query query = session.createQuery(
                "from PizzaEntity lpe"
        );
        session.getTransaction().commit();
        List result = query.getResultList();
        if(!result.isEmpty())
            lpe = result;
        sessionCheck(session);
        return lpe;
    }

    public List<PizzaEntity> getPizzasByUser(UserEntity userEntity) throws Exception {
        Session session = null;
        List<PizzaEntity> pizzaEntities = null;
        session = HibernateMain.getSession();
        session.beginTransaction();
        Query query = session.createQuery(
                "select pe from OrdersEntity oe inner " +
                        "join PizzaEntity pe " +
                        "on oe.idPizza=pe.pizzaId " +
                        "where oe.idUser='"+userEntity.getUserId()+"'"
        );
        session.getTransaction().commit();
        pizzaEntities = query.getResultList();
        sessionCheck(session);
        return pizzaEntities;
    }


    public void addToCart(String pName, UserEntity ue) throws Exception {
        Session session = null;
        OrdersEntity oe = null;
        PizzaEntity pe = null;
            session = HibernateMain.getSession();
            session.beginTransaction();
            oe = new OrdersEntity();
            pe = getPizzaByName(pName);
            oe.setIdPizza(pe.getPizzaId());
            oe.setIdUser(ue.getUserId());
            session.save(oe);
            session.getTransaction().commit();
            sessionCheck(session);
    }

    public void addToCart(List<String> pName, UserEntity ue) throws Exception {
        Session session = null;
        OrdersEntity ordersEntity = null;
        PizzaEntity pizzaEntity  = null;
        List<PizzaEntity> pizzaEntities = new ArrayList<PizzaEntity>();
        session = HibernateMain.getSession();
        session.beginTransaction();
        for (Iterator i = pName.iterator(); i.hasNext();
             ) {
            String pizzaName = (String)i.next();
            pizzaEntity = getPizzaByName(pizzaName);
            ordersEntity = new OrdersEntity();
            ordersEntity.setIdPizza(pizzaEntity.getPizzaId());
            ordersEntity.setIdUser(ue.getUserId());
            session.save(ordersEntity);

        }
        session.getTransaction().commit();
        sessionCheck(session);
    }

    public void deleteFromCart(PizzaEntity pe, UserEntity ue) throws Exception {
        Session session = null;
        OrdersEntity oe = null;
            session = HibernateMain.getSession();
            session.beginTransaction();
            oe = new OrdersEntity();
            oe.setIdUser(ue.getUserId());
            oe.setIdPizza(pe.getPizzaId());
            session.delete(oe);
            session.getTransaction().commit();
            sessionCheck(session);
    }


    public void deleteUser(UserEntity userEntity) throws Exception {
        Session session = null;
        session = HibernateMain.getSession();
        session.beginTransaction();
        session.delete(userEntity);
        session.getTransaction().commit();
        sessionCheck(session);
    }

    private void sessionCheck(Session session) {
        if (session != null && session.isOpen()) {

            session.close();
        }
    }
}
