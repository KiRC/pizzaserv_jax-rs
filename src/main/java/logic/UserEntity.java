package logic;

import com.alibaba.fastjson.annotation.JSONField;
import com.mysql.management.util.Str;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "rest_pizzadb", catalog = "")
public class UserEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;
    private String phone;
    private String userName;

    @JSONField(name = "userName")
    @Basic
    @Column(name = "userName")
    public String getUserName(){return userName;}

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @JSONField(name = "phone")
    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (userId != that.userId) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}
