package logic;

import logic.PizzaEntity;
import logic.UserEntity;
import logic.OrdersEntity;

import java.sql.SQLException;
import java.util.List;

public interface IBaseMethods {
    //public boolean userExist(String phone) throws Exception;
    public void addToCart (String pName, UserEntity ue) throws Exception;
    public void addToCart (List<String> pName, UserEntity ue) throws Exception;
    public UserEntity addPhoneNumber (String userName, String phone) throws Exception;
    public void deleteFromCart (PizzaEntity pe, UserEntity ue) throws  Exception;
    public List<PizzaEntity> getPizzasList() throws Exception;
    public UserEntity getUserByName(String userName) throws Exception;
    public List<PizzaEntity> getPizzasByUser(UserEntity userEntity) throws Exception;
}
