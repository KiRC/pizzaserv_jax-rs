package logic;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;


@Entity
@Table(name = "pizza", schema = "rest_pizzadb", catalog = "")
public class PizzaEntity {
    private int pizzaId;
    private String name;
    private double price;

    @Id
    @Column(name = "pizza_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(int pizzaId) {
        this.pizzaId = pizzaId;
    }

    @JSONField(name = "Name")
    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JSONField(name = "Price")
    @Basic
    @Column(name = "price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PizzaEntity that = (PizzaEntity) o;

        if (pizzaId != that.pizzaId) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = pizzaId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
