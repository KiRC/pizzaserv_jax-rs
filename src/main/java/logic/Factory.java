import logic.BaseMethods;

public class Factory {

    private static BaseMethods baseMethods = null;
    private static Factory instance = null;

    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public BaseMethods getBaseMethods() {
        if (baseMethods == null) {
            baseMethods = new BaseMethods();
        }
        return baseMethods;
    }

}