package JSONEntity;

import com.alibaba.fastjson.annotation.JSONField;
import logic.PizzaEntity;

public class JSONPizzaEntity {
    private String name;
    private double price;


    public JSONPizzaEntity(PizzaEntity pizzaEntity){
        this.name = pizzaEntity.getName();
        this.price = pizzaEntity.getPrice();
    }

    public JSONPizzaEntity(){}

    @JSONField(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JSONField(name = "Price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
