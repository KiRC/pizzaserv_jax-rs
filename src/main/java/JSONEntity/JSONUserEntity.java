package JSONEntity;

import com.alibaba.fastjson.annotation.JSONField;
import logic.UserEntity;


public class JSONUserEntity {

    private String phone;
    private String userName;

    public JSONUserEntity(UserEntity userEntity){
        this.phone = userEntity.getPhone();
        this.userName = userEntity.getUserName();
    }

    public JSONUserEntity(){}

    @JSONField(name = "userName")
    public String getUserName(){return userName;}

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JSONField(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
