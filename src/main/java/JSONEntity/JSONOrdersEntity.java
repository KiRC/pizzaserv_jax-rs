package JSONEntity;


import com.alibaba.fastjson.annotation.JSONField;
import logic.OrdersEntity;

public class JSONOrdersEntity {

    @JSONField(name = "OrderID")
    private int orderId;

    @JSONField(name = "PizzaName")
    private String pizzaName;
    @JSONField(name = "UserName")
    private String userName;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
