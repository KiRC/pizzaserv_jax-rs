
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.mysql.management.util.Str;
import logic.IBaseMethods;
import logic.PizzaEntity;
import logic.UserEntity;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.jws.soap.SOAPBinding;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import JSONEntity.*;


// The Java class will be hosted at the URI path "/helloworld"
@Path("/index")
public class UserResource {
    private IBaseMethods methods;

    @Context
    private HttpHeaders requestHeaders;

    private String getHeaderVersion(){
        return requestHeaders.getRequestHeader("version").get(0);
    }

    // The Java method will process HTTP GET requests
    /*@GET
    @Produces(value = "application/json")
    public String getPizzas() throws Exception{
    methods = Factory.getInstance().getBaseMethods();
    List<PizzaEntity> pizzaEntities = methods.getPizzasList();
    return JSON.toJSONString(pizzaEntities);
    }*/

    //вывод списка всех пиз
    @GET
    @Produces(value = "application/json")
    public Response getPizzas() throws Exception{
        methods = Factory.getInstance().getBaseMethods();
        List<PizzaEntity> pizzaEntities = methods.getPizzasList();
        List<JSONPizzaEntity> jsonPizzaEntities = new ArrayList<JSONPizzaEntity>();
        for (Iterator i = pizzaEntities.iterator(); i.hasNext();){
            PizzaEntity pizzaEntity = (PizzaEntity)i.next();
            jsonPizzaEntities.add(
                    new JSONPizzaEntity(pizzaEntity)
            );
        }
        if(jsonPizzaEntities.isEmpty())
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Pizzas not found!")
                    .build();
        return Response.ok(
                JSON.toJSONString(jsonPizzaEntities)
        ).build();
    }

    //регистрация
    @POST
    @Consumes(value="application/json")
    public Response registrateUser(String inputJSON) throws Exception {
        JSONUserEntity userJSON = JSON.parseObject(inputJSON, JSONUserEntity.class);
        UserEntity userEntity = new UserEntity();
        userEntity.setPhone(userJSON.getPhone());
        userEntity.setUserName(userJSON.getUserName());
        methods = Factory.getInstance().getBaseMethods();
        methods.addPhoneNumber(userEntity.getUserName(), userEntity.getPhone());
        return Response.ok().build();
    }

 /*   @GET
    @Path("/{userName}")
    @Produces(value = "application/json")
    public Response getUser(@PathParam("userName") String userName) throws Exception{
        methods = Factory.getInstance().getBaseMethods();
        UserEntity userEntity = methods.getUserByName(userName);
        JSONUserEntity jsonUserEntity = new JSONUserEntity(userEntity);
        if(userEntity==null)
            return Response.status(Response.Status.NOT_FOUND).entity("User not exist").build();
        return Response.ok(JSON.toJSONString(jsonUserEntity)).build();
    }*/

    //для добавления пост для изменения пут
    //это просто логирование
    @PUT
    @Path("/{userName}")
    @Produces(value = "application/json")
    @Consumes(value = "application/json")
    public Response logUser(@PathParam("userName") String userName) throws Exception{
        methods = Factory.getInstance().getBaseMethods();
        UserEntity userEntity = methods.getUserByName(userName);
        JSONUserEntity jsonUserEntity = new JSONUserEntity(userEntity);
        if(userEntity==null)
            return Response.status(Response.Status.NOT_FOUND).entity("User not exist").build();

        return Response.ok(JSON.toJSONString(jsonUserEntity)).build();
    }

    @POST
    @Path("/{userName}/orders")
    @Consumes(value = "application/json")
    public Response addToStock(@PathParam("userName") String userName,
                               String inputJSON) throws Exception {
        methods = Factory.getInstance().getBaseMethods();
        List<PizzaEntity> pizzaEntities = methods.getPizzasList();
        TypeReference<List<JSONPizzaEntity>> type = new TypeReference<List<JSONPizzaEntity>>() {
        };

        List<JSONPizzaEntity> jsonPizzaEntities =
                JSON.parseObject(inputJSON, type);

        UserEntity userEntity = methods.getUserByName(userName);
        List<String> pName = new ArrayList<String>();

        for (Iterator i = jsonPizzaEntities.iterator(); i.hasNext(); ) {
            JSONPizzaEntity pizzaEntity = (JSONPizzaEntity) i.next();
            pName.add(pizzaEntity.getName());
        }

        methods.addToCart(pName, userEntity);
        return Response.ok().build();
    }

    @GET
    @Path("/{userName}/orders")
    @Produces(value = "application/json")
    public Response getMyOrders(@PathParam("userName") String userName) throws Exception{
        methods = Factory.getInstance().getBaseMethods();
        UserEntity userEntity  =null;
        userEntity = methods.getUserByName(userName);
        List<JSONPizzaEntity> jsonPizzaEntities = new ArrayList<JSONPizzaEntity>();
        List<PizzaEntity> pizzaEntities = methods.getPizzasByUser(userEntity);
        for (Iterator i = pizzaEntities.iterator(); i.hasNext();){
            Object pizzaEntity = i.next();
            jsonPizzaEntities.add(
                    new JSONPizzaEntity((PizzaEntity)pizzaEntity)
            );
        }
        if(jsonPizzaEntities.isEmpty())
            return Response.ok("Cart is empty").build();
    return  Response.ok(JSON.toJSONString(jsonPizzaEntities)).build();
    }
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    public String getClichedMessage() {
        // Return some cliched textual content
        return "Hello World";
    }


}